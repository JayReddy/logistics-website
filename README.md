# Logistics Website

This website is a logistics platform designed and developed using React and styled with Chakra UI. It features a modern and responsive design, optimized for various devices.

## Features

- **Quotation Requests**: Clients can easily submit quotation requests directly to the company's email through the website.
- **Service Overview**: Get detailed information about the services offered by the company, including transportation, warehousing, and supply chain solutions,etc.
- **Responsive Design**: The website is designed to provide a seamless user experience across desktop, tablet, and mobile devices.

## Website Link

The website is live and can be accessed [https://www.globexuae.com/].

---

**Note:** This README file provides an overview of the features of the logistics website.
